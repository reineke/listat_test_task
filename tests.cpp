#include <gtest/gtest.h>
#include "dummy_xml.h"
#include "prime_finder.h"
#include "xml_prime_finder.h"

TEST(DummyXmlTest, ElementCreated) {
	DummyXml xml("root", "test xml");
	auto const & attr = xml.get_attributes();
	auto const & elems = xml.get_elements();
	EXPECT_EQ(attr.size(), 0);
	EXPECT_EQ(elems.size(), 0);
	EXPECT_TRUE(xml.get_name() == "root");
	EXPECT_TRUE(xml.get_text() == "test xml");
}

TEST(DummyXmlTest, ElementAddText) {
	DummyXml xml("root", "test xml");
	auto const & attr = xml.get_attributes();
	EXPECT_TRUE(xml.get_text() == "test xml");
	xml.set_text("new text");
	EXPECT_TRUE(xml.get_text() == "new text");
}

TEST(DummyXmlTest, AddOneAttribute) {
	DummyXml xml("root", "test xml");
	xml.add_attribute("foo", "bar");
	DummyXml::attribute test_pair("foo", "bar");
	auto const & attr = xml.get_attributes();
	EXPECT_TRUE(attr[0] == test_pair);
	EXPECT_EQ(attr.size(), 1);
}

TEST(DummyXmlTest, AddTwoAttributes) {
	DummyXml xml("root", "test xml");
	xml.add_attribute("foo", "bar");
	xml.add_attribute("fuz", "baz");
	DummyXml::attribute test_pair1("foo", "bar");
	DummyXml::attribute test_pair2("fuz", "baz");
	auto const & attr= xml.get_attributes();
	EXPECT_TRUE(attr[0] == test_pair1);
	EXPECT_TRUE(attr[1] == test_pair2);
	EXPECT_EQ(attr.size(), 2);
}

TEST(DummyXmlTest, DeleteFirstAttribute) {
	DummyXml xml("root", "test xml");
	xml.add_attribute("foo", "bar");
	xml.add_attribute("fuz", "baz");
	DummyXml::attribute test_pair("fuz", "baz");
	auto const & attr = xml.get_attributes();
	auto it = attr.begin();
	xml.remove_attribute(it);
	EXPECT_TRUE(attr[0] == test_pair);
	EXPECT_EQ(attr.size(), 1);
}

TEST(DummyXmlTest, DeleteSecondAttribute) {
	DummyXml xml("root", "test xml");
	xml.add_attribute("foo", "bar");
	xml.add_attribute("fuz", "baz");
	DummyXml::attribute test_pair("foo", "bar");
	auto const & attr = xml.get_attributes();
	auto it = attr.begin() + 1;
	xml.remove_attribute(it);
	EXPECT_TRUE(attr[0] == test_pair);
	EXPECT_EQ(attr.size(), 1);
}

TEST(DummyXmlTest, DeleteAllAttributes) {
	DummyXml xml("root", "test xml");
	xml.add_attribute("foo", "bar");
	xml.add_attribute("fuz", "baz");
	auto const & attr = xml.get_attributes();
	for (auto it = attr.begin(); it != attr.end(); it = xml.remove_attribute(it))
		;
	EXPECT_EQ(attr.size(), 0);
}

TEST(DummyXmlTest, AddOneElement) {
	DummyXml xml("root", "test xml");
	DummyXml test_elem("foo", "bar");
	xml.add_element(test_elem);
	auto const & elems = xml.get_elements();
	EXPECT_TRUE(elems[0] == test_elem);
	EXPECT_EQ(elems.size(), 1);
}

TEST(DummyXmlTest, AddTwoElements) {
	DummyXml xml("root", "test xml");
	DummyXml test_elem1("foo");
	DummyXml test_elem2("bar");
	xml.add_element(test_elem1);
	xml.add_element(test_elem2);
	auto const & elems = xml.get_elements();
	EXPECT_TRUE(elems[0] == test_elem1);
	EXPECT_TRUE(elems[1] == test_elem2);
	EXPECT_EQ(elems.size(), 2);
}

TEST(DummyXmlTest, DeleteFirstElement) {
	DummyXml xml("root", "test xml");
	DummyXml test_elem1("foo");
	DummyXml test_elem2("bar");
	xml.add_element(test_elem1);
	xml.add_element(test_elem2);
	auto const & elems = xml.get_elements();
	auto it = elems.begin();
	xml.remove_element(it);
	EXPECT_TRUE(elems[0] == test_elem2);
	EXPECT_EQ(elems.size(), 1);
}

TEST(DummyXmlTest, DeleteSecondElement) {
	DummyXml xml("root", "test xml");
	DummyXml test_elem1("foo");
	DummyXml test_elem2("bar");
	xml.add_element(test_elem1);
	xml.add_element(test_elem2);
	auto const & elems = xml.get_elements();
	auto it = elems.begin() + 1;
	xml.remove_element(it);
	EXPECT_TRUE(elems[0] == test_elem1);
	EXPECT_EQ(elems.size(), 1);
}

TEST(DummyXmlTest, DeleteAllElements) {
	DummyXml xml("root", "test xml");
	DummyXml test_elem1("foo");
	DummyXml test_elem2("bar");
	xml.add_element(test_elem1);
	xml.add_element(test_elem2);
	auto const & elems = xml.get_elements();
	for (auto it = elems.begin(); it != elems.end(); it = xml.remove_element(it))
		;
	EXPECT_EQ(elems.size(), 0);
}

TEST(DummyXmlTest, XmlStreamOneEmptyElement) {
	DummyXml xml("root");
	std::stringstream xml_stream, test_stream;
	xml_stream << xml;
	test_stream << "<root/>" << std::endl;
	EXPECT_EQ(xml_stream.str(), test_stream.str());
}

TEST(DummyXmlTest, XmlStreamOneElementWithText) {
	DummyXml xml("root", "text");
	std::stringstream xml_stream, test_stream;
	xml_stream << xml;
	test_stream << "<root>text</root>" << std::endl;
	EXPECT_EQ(xml_stream.str(), test_stream.str());
}

TEST(DummyXmlTest, XmlStreamOneElementWithAttribute) {
	DummyXml xml("root");
	std::stringstream xml_stream, test_stream;
	xml.add_attribute("foo", "bar");
	xml_stream << xml;
	test_stream << "<root foo=\"bar\"/>" << std::endl;
	EXPECT_EQ(xml_stream.str(), test_stream.str());
}

TEST(DummyXmlTest, XmlStreamOneElementWithAttributeWithQuote) {
	DummyXml xml("root");
	std::stringstream xml_stream, test_stream;
	xml.add_attribute("foo", "b\"ar");
	xml_stream << xml;
	test_stream << "<root foo='b\"ar'/>" << std::endl;
	EXPECT_EQ(xml_stream.str(), test_stream.str());
}

TEST(DummyXmlTest, XmlStreamOneElementWithTwoAttributes) {
	DummyXml xml("root");
	std::stringstream xml_stream, test_stream;
	xml.add_attribute("foo", "bar");
	xml.add_attribute("fuz", "baz");
	xml_stream << xml;
	test_stream << "<root foo=\"bar\" fuz=\"baz\"/>" << std::endl;
	EXPECT_EQ(xml_stream.str(), test_stream.str());
}

TEST(DummyXmlTest, XmlStreamOneElementWithAttributeAndText) {
	DummyXml xml("root", "text");
	std::stringstream xml_stream, test_stream;
	xml.add_attribute("foo", "bar");
	xml_stream << xml;
	test_stream << "<root foo=\"bar\">text</root>" << std::endl;
	EXPECT_EQ(xml_stream.str(), test_stream.str());
}

TEST(DummyXmlTest, XmlStreamOneElementWithNestedElement) {
	DummyXml xml("root");
	DummyXml nested_xml("nested");
	std::stringstream xml_stream, test_stream;
	xml.add_element(nested_xml);
	xml_stream << xml;
	test_stream << "<root>" << std::endl
				<< "\t<nested/>" << std::endl
				<< "</root>" << std::endl;
	EXPECT_EQ(xml_stream.str(), test_stream.str());
}

TEST(DummyXmlTest, XmlStreamOneElementWithNestedElementAndText) {
	DummyXml xml("root", "text");
	DummyXml nested_xml("nested");
	std::stringstream xml_stream, test_stream;
	xml.add_element(nested_xml);
	xml_stream << xml;
	test_stream << "<root>" << std::endl
				<< "\ttext" << std::endl
				<< "\t<nested/>" << std::endl
				<< "</root>" << std::endl;
	EXPECT_EQ(xml_stream.str(), test_stream.str());
}

TEST(DummyXmlTest, NameWithSpaceException) {
	EXPECT_NO_THROW(DummyXml("root"));
	EXPECT_THROW(DummyXml("ro ot"), std::invalid_argument);
	EXPECT_THROW(DummyXml(" root"), std::invalid_argument);
	EXPECT_THROW(DummyXml("root "), std::invalid_argument);
}

TEST(DummyXmlTest, NameStartsWithWrongCharException) {
	EXPECT_NO_THROW(DummyXml("_root"));
	EXPECT_THROW(DummyXml("1root"), std::invalid_argument);
	EXPECT_NO_THROW(DummyXml("r1oot"));
	EXPECT_THROW(DummyXml("+root"), std::invalid_argument);
	EXPECT_NO_THROW(DummyXml("r+oot"));
}

TEST(DummyXmlTest, TextWithTwoQuoteTypesException) {
	DummyXml xml("root");
	EXPECT_NO_THROW(xml.set_text("\"text\""));
	EXPECT_NO_THROW(xml.set_text("'text'"));
	EXPECT_THROW(xml.set_text("'\"text\"'"), std::invalid_argument);
	EXPECT_THROW(DummyXml("root", "'\"text\"'"), std::invalid_argument);
}

TEST(DummyXmlTest, ReadEmptyXmlFromStream) {
	std::stringstream source_stream("<root></root>"), test_stream, xml_stream;
	DummyXml xml(source_stream);
	test_stream << "<root/>" << std::endl;
	xml_stream << xml;
	EXPECT_EQ(xml_stream.str(), test_stream.str());
}

TEST(DummyXmlTest, ReadEmptySelfClosedXmlFromStream) {
	std::stringstream source_stream("<root/>"), xml_stream, test_stream;
	DummyXml xml(source_stream);
	test_stream << "<root/>" << std::endl;
	xml_stream << xml;
	EXPECT_EQ(xml_stream.str(), test_stream.str());
}

TEST(DummyXmlTest, ReadXmlWithTextFromStream) {
	std::stringstream source_stream("<root>text in root</root>"), test_stream, xml_stream;
	DummyXml xml(source_stream);
	test_stream << "<root>text in root</root>" << std::endl;
	xml_stream << xml;
	EXPECT_EQ(xml_stream.str(), test_stream.str());
}

TEST(DummyXmlTest, ReadNestedXmlFromStream) {
	std::stringstream source_stream("<root><alone/></root>"), test_stream, xml_stream;
	DummyXml xml(source_stream);
	test_stream << "<root>" << std::endl
				<< "\t<alone/>" << std::endl
				<< "</root>" << std::endl;
	xml_stream << xml;
	EXPECT_EQ(xml_stream.str(), test_stream.str());
}

TEST(DummyXmlTest, ReadNestedAndTextXmlFromStream) {
	std::stringstream source_stream("<root><alone/>text</root>"), test_stream, xml_stream;
	DummyXml xml(source_stream);
	test_stream << "<root>" << std::endl
				<< "\ttext" << std::endl
				<< "\t<alone/>" << std::endl
				<< "</root>" << std::endl;
	xml_stream << xml;
	EXPECT_EQ(xml_stream.str(), test_stream.str());
}

TEST(DummyXmlTest, ReadNestedAndMoreTextXmlFromStream) {
	std::stringstream source_stream("<root>some text<alone/>more text</root>"), test_stream, xml_stream;
	DummyXml xml(source_stream);
	test_stream << "<root>" << std::endl
				<< "\tsome text" << std::endl
				<< "more text" << std::endl
				<< "\t<alone/>" << std::endl
				<< "</root>" << std::endl;
	xml_stream << xml;
	EXPECT_EQ(xml_stream.str(), test_stream.str());
}

TEST(DummyXmlTest, ReadNestedElementXmlFromStream) {
	std::stringstream source_stream("<root><element></element></root>"), test_stream, xml_stream;
	DummyXml xml(source_stream);
	test_stream << "<root>" << std::endl
				<< "\t<element/>" << std::endl
				<< "</root>" << std::endl;
	xml_stream << xml;
	EXPECT_EQ(xml_stream.str(), test_stream.str());
}

TEST(DummyXmlTest, ReadNestedElementWithTextXmlFromStream) {
	std::stringstream source_stream("<root><element>some text</element></root>"), test_stream, xml_stream;
	DummyXml xml(source_stream);
	test_stream << "<root>" << std::endl
				<< "\t<element>some text</element>" << std::endl
				<< "</root>" << std::endl;
	xml_stream << xml;
	EXPECT_EQ(xml_stream.str(), test_stream.str());
}

TEST(DummyXmlTest, ReadNestedElementWithSelfClosedElementXmlFromStream) {
	std::stringstream source_stream("<root><element><alone/></element></root>"), test_stream, xml_stream;
	DummyXml xml(source_stream);
	test_stream << "<root>" << std::endl
				<< "\t<element>" << std::endl
				<< "\t\t<alone/>" << std::endl
				<< "\t</element>" << std::endl
				<< "</root>" << std::endl;
	xml_stream << xml;
	EXPECT_EQ(xml_stream.str(), test_stream.str());
}

TEST(DummyXmlTest, ReadNestedNestedElementXmlFromStream) {
	std::stringstream source_stream("<root><element><more_nested_elements>and more text</more_nested_elements></element></root>"), test_stream, xml_stream;
	DummyXml xml(source_stream);
	test_stream << "<root>" << std::endl
				<< "\t<element>" << std::endl
				<< "\t\t<more_nested_elements>and more text</more_nested_elements>" << std::endl
				<< "\t</element>" << std::endl
				<< "</root>" << std::endl;
	xml_stream << xml;
	EXPECT_EQ(xml_stream.str(), test_stream.str());
}

TEST(DummyXmlTest, ReadBigXmlFromStream) {
	std::stringstream source_stream("<root><element><more_nested_elements>and more text</more_nested_elements></element><one_more_element>and text too</one_more_element></root>"), test_stream, xml_stream;
	DummyXml xml(source_stream);
	test_stream << "<root>" << std::endl
				<< "\t<element>" << std::endl
				<< "\t\t<more_nested_elements>and more text</more_nested_elements>" << std::endl
				<< "\t</element>" << std::endl
				<< "\t<one_more_element>and text too</one_more_element>" << std::endl
				<< "</root>" << std::endl;
	xml_stream << xml;
	EXPECT_EQ(xml_stream.str(), test_stream.str());
}

TEST(PrimeFinderTest, CreatePrimeFinderWithIntervals) {
	std::vector<PrimeFinder::interval> test_intervals;
	test_intervals.push_back(PrimeFinder::interval(1, 2));
	test_intervals.push_back(PrimeFinder::interval(6, 15));
	PrimeFinder finder(test_intervals);
	auto const & intervals = finder.get_intervals();
	EXPECT_TRUE(intervals[0] == test_intervals[0]);
	EXPECT_TRUE(intervals[1] == test_intervals[1]);
	EXPECT_EQ(intervals.size(), 2);
}

TEST(PrimeFinderTest, AddOneInterval) {
	PrimeFinder finder;
	finder.add_interval(1, 2);
	auto const & intervals = finder.get_intervals();
	EXPECT_TRUE(intervals[0] == PrimeFinder::interval(1, 2));
	EXPECT_EQ(intervals.size(), 1);
}

TEST(PrimeFinderTest, AddTwoIntervals) {
	PrimeFinder finder;
	finder.add_interval(1, 2);
	finder.add_interval(14, 89);
	auto const & intervals = finder.get_intervals();
	EXPECT_TRUE(intervals[0] == PrimeFinder::interval(1, 2));
	EXPECT_TRUE(intervals[1] == PrimeFinder::interval(14, 89));
	EXPECT_EQ(intervals.size(), 2);
}

TEST(PrimeFinderTest, DeleteFirstInterval) {
	PrimeFinder finder;
	finder.add_interval(1, 2);
	finder.add_interval(14, 89);
	auto const & intervals = finder.get_intervals();
	auto it = intervals.begin();
	finder.remove_interval(it);
	EXPECT_TRUE(intervals[0] == PrimeFinder::interval(14, 89));
	EXPECT_EQ(intervals.size(), 1);
}

TEST(PrimeFinderTest, DeleteSecondInterval) {
	PrimeFinder finder;
	finder.add_interval(1, 2);
	finder.add_interval(14, 89);
	auto const & intervals = finder.get_intervals();
	auto it = intervals.begin() + 1;
	finder.remove_interval(it);
	EXPECT_TRUE(intervals[0] == PrimeFinder::interval(1, 2));
	EXPECT_EQ(intervals.size(), 1);
}

TEST(PrimeFinderTest, DeleteAllInterval) {
	PrimeFinder finder;
	finder.add_interval(1, 2);
	finder.add_interval(14, 89);
	auto const & intervals = finder.get_intervals();
	for (auto it = intervals.begin();
			it != intervals.end();
			it = finder.remove_interval(it))
		;
	EXPECT_EQ(intervals.size(), 0);
}

TEST(PrimeFinderTest, addInvalidInterval) {
	PrimeFinder finder;
	EXPECT_THROW(finder.add_interval(6, 1), std::invalid_argument);
}

TEST(PrimeFinderTest, addInvalidIntervalInConstructor) {
	EXPECT_THROW(PrimeFinder(std::vector<PrimeFinder::interval>(1, PrimeFinder::interval(11, 2))),
			std::invalid_argument);
}

TEST(PrimeFinderTest, getOneResult) {
	PrimeFinder finder;
	std::stringstream primes_stream, test_stream;
	finder.add_interval(1, 12);
	finder.start();
	finder.stop();
	auto results = finder.get_results();
	EXPECT_EQ(results.size(), 1);
	EXPECT_EQ(results[0].first.first, 1);
	EXPECT_EQ(results[0].first.second, 12);
	for (auto const & num : results[0].second)
		test_stream << ((&num == &*results[0].second.begin()) ? "" : " ") << num;
	primes_stream << "2 3 5 7 11";
	EXPECT_EQ(primes_stream.str(), test_stream.str());
}

TEST(XmlPrimeFinderTest, AddValidXml1) {
	std::stringstream source_stream("<root><intervals><interval><low>1</low><high>10</high></interval></intervals></root>");
	std::stringstream test_stream, xml_stream;
	DummyXml xml(source_stream);
	XmlPrimeFinder finder(xml);
	finder.start();
	finder.stop();
	auto results = finder.get_results();
	test_stream << "<root>" << std::endl
				<< "\t<intervals>" << std::endl
				<< "\t\t<interval>" << std::endl
				<< "\t\t\t<low>1</low>" << std::endl
				<< "\t\t\t<high>10</high>" << std::endl
				<< "\t\t\t<primes>2 3 5 7</primes>" << std::endl
				<< "\t\t</interval>" << std::endl
				<< "\t</intervals>" << std::endl
				<< "</root>" << std::endl;
	xml_stream << results;
	EXPECT_EQ(test_stream.str(), xml_stream.str());
}

TEST(XmlPrimeFinderTest, AddValidXml2) {
	std::stringstream source_stream("<root><intervals><interval><low>1</low><high>10</high></interval></intervals></root>");
	std::stringstream test_stream, xml_stream;
	DummyXml xml(source_stream);
	XmlPrimeFinder finder;
	finder.add_interval(xml);
	finder.start();
	finder.stop();
	auto results = finder.get_results();
	test_stream << "<root>" << std::endl
				<< "\t<intervals>" << std::endl
				<< "\t\t<interval>" << std::endl
				<< "\t\t\t<low>1</low>" << std::endl
				<< "\t\t\t<high>10</high>" << std::endl
				<< "\t\t\t<primes>2 3 5 7</primes>" << std::endl
				<< "\t\t</interval>" << std::endl
				<< "\t</intervals>" << std::endl
				<< "</root>" << std::endl;
	xml_stream << results;
	EXPECT_EQ(test_stream.str(), xml_stream.str());
}

TEST(XmlPrimeFinderTest, AddNegativeInterval) {
	std::stringstream source_stream("<root><intervals><interval><low>-1</low><high>10</high></interval></intervals></root>");
	std::stringstream test_stream, xml_stream;
	DummyXml xml(source_stream);
	XmlPrimeFinder finder;
	EXPECT_THROW(finder.add_interval(xml), std::invalid_argument);
}

TEST(XmlPrimeFinderTest, NoLowBound) {
	std::stringstream source_stream("<root><intervals><interval><high>10</high></interval></intervals></root>");
	std::stringstream test_stream, xml_stream;
	DummyXml xml(source_stream);
	XmlPrimeFinder finder;
	EXPECT_THROW(finder.add_interval(xml), std::invalid_argument);
}

TEST(XmlPrimeFinderTest, NoHighBound) {
	std::stringstream source_stream("<root><intervals><interval><low>1</low></interval></intervals></root>");
	std::stringstream test_stream, xml_stream;
	DummyXml xml(source_stream);
	XmlPrimeFinder finder;
	EXPECT_THROW(finder.add_interval(xml), std::invalid_argument);
}

int main() {
	testing::InitGoogleTest();
	return RUN_ALL_TESTS();
}