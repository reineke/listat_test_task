#include <iostream>
#include <fstream>
#include "dummy_xml.h"
#include "xml_prime_finder.h"

int handle_input_file(char *filename, XmlPrimeFinder & finder) {
	std::ifstream ifs;
	try {
		ifs.open(filename);
	} catch (const std::ifstream::failure & e) {
		std::cout << "Unable to open file: " << filename << std::endl;
		return 1;
	}
	if (!ifs) {
		std::cout << "File \"" << filename << "\" does not exist." << std::endl;
		return 1;
	}
	try {
		DummyXml input(ifs);
		finder.add_interval(input);
	} catch (const std::exception & e) {
		std::cout << e.what() << std::endl;
		return 2;
	}
	return 0;
}

int main(int ac, char *av[]) {
	int exit_code = 0;
	XmlPrimeFinder finder;
	for (int i = 1; i < ac; i++) {
		exit_code = handle_input_file(av[i], finder);
		if (exit_code)
			return exit_code;
	}
	finder.start();
	finder.stop();
	auto result = finder.get_results();
	std::ofstream ofs;
	try {
		ofs.open("result.xml");
		ofs << result;
	} catch (const std::ifstream::failure & e) {
		std::cout << "Unable to write result file." << std::endl;
		return 1;
	}
	return exit_code;
}
