cmake_minimum_required(VERSION 3.5)
project(listat_test_task)

set(CMAKE_CXX_STANDARD 11)

set(gtest_SOURCE_DIR "googletest/googletest")

set(COMMON_SOURCES
        dummy_xml.cpp
        prime_finder.cpp
        xml_prime_finder.cpp)

add_executable(find_primes find_primes.cpp ${COMMON_SOURCES})

add_executable(tests tests.cpp ${COMMON_SOURCES})
include_directories(${gtest_SOURCE_DIR}/include)
add_subdirectory(${gtest_SOURCE_DIR})
target_link_libraries(tests gtest gtest_main)