#pragma once

#include <vector>
#include <thread>
#include <mutex>
#include <exception>

class PrimeFinder {

public:
	typedef std::pair<unsigned int, unsigned int> interval;
	typedef std::vector<unsigned int> primes;
	typedef std::pair<interval, primes> result;

	explicit PrimeFinder(std::vector<interval> const &);

	PrimeFinder() = default;
	~PrimeFinder() = default;
	PrimeFinder(PrimeFinder const &) = delete;

	void add_interval(unsigned int, unsigned int);
	std::vector<interval> const & get_intervals() const;
	std::vector<interval>::const_iterator remove_interval(
			std::vector<interval>::const_iterator);
	std::vector<result> get_results();
	void start();
	void stop();

private:
	std::vector<interval> _intervals;
	std::vector<std::thread> _threads;
	std::vector<result> _results;
	std::mutex _results_mutex;

	void _finder_thread(interval);

};