#include "prime_finder.h"

PrimeFinder::PrimeFinder(std::vector<interval> const & intervals) {
	for (auto const & it : intervals)
		this->add_interval(it.first, it.second);
}

void PrimeFinder::add_interval(unsigned int low, unsigned int high) {
	if (low > high)
		throw std::invalid_argument(
				"First value of interval must be lower than second.");
	this->_intervals.emplace_back(low, high);
}

std::vector<PrimeFinder::interval> const & PrimeFinder::get_intervals() const {
	return this->_intervals;
}

std::vector<PrimeFinder::interval>::const_iterator PrimeFinder::remove_interval(
		std::vector<interval>::const_iterator it) {
	return this->_intervals.erase(it);
}

static bool is_prime(unsigned int const & num) {
	if (num <= 1)
		return false;
	for (unsigned int i = 2; i <= num / 2; i++){
		if (num % i == 0)
			return false;
	}
	return true;
}

void PrimeFinder::_finder_thread(interval num) {
	primes result_nums;
	for (auto i = num.first; i < num.second; i++) {
		if (is_prime(i))
			result_nums.push_back(i);
	}
	this->_results_mutex.lock();
	this->_results.emplace_back(num, result_nums);
	this->_results_mutex.unlock();
}

std::vector<PrimeFinder::result> PrimeFinder::get_results() {
	auto results = this->_results;
	this->_results.clear();
	return results;
}

void PrimeFinder::start() {
	for (auto it = this->_intervals.begin();
			it != this->_intervals.end();
			it = this->_intervals.erase(it)) {
		this->_threads.emplace_back(&PrimeFinder::_finder_thread, this, *it);
	}
}

void PrimeFinder::stop() {
	for (auto & it : this->_threads) {
		it.join();
	}
}