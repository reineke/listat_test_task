#pragma once

#include "prime_finder.h"
#include "dummy_xml.h"

class XmlPrimeFinder : public PrimeFinder {

public:
	explicit XmlPrimeFinder(DummyXml const &);

	XmlPrimeFinder() = default;
	~XmlPrimeFinder() = default;
	XmlPrimeFinder(XmlPrimeFinder const &) = delete;

	void add_interval(DummyXml const &);
	DummyXml get_results();

};